const mongoose = require ('mongoose');

const Schema = mongoose.Schema;

//create Schema 

const ItemSchema = new Schema({
    name:{
        type: String,
        required: false
    },
    surname:{
        type: String,
        required: false
    },
    email:{
        type: String,
        required: false
    },
    phone_number:{
        type: Number,
        required: false
    },
    createdDate:{
        type: Date,
        default: Date.now
    }
});

module.exports = Item = mongoose.model( 'item', ItemSchema);